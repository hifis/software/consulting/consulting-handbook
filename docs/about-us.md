---
title: About Us
---

# About us

[![HIFIS Logo with Claim](images/HIFIS_logo_claim_blue.svg){style="display: left; margin: 0 auto; width:55%"}][hifis]

## Mission Statement

HIFIS stands for Helmholtz Federated IT Services.

The top priority of Helmholtz research is cross-centre and international cooperation and common access to data treasure and services.
At the same time, the significance of sustainable software development for the research process is recognised.

**HIFIS builds and sustains an excellent IT infrastructure connecting all Helmholtz research fields and centres.**

Further details can be found on the [HIFIS website][mission].

The overall mission of HIFIS Consulting is adopted from the [HGF software policy][policy] to the specific needs.
It ensures efficient and effective sustainable state-of-the-art software development and data science.
The HGF software policy defines the scope and serves as a guiding framework to identify and prioritise topics that are addressed by the consulting service.

## Working Areas

In order to achieve aims derived from the mission statement, different complementing instruments and measures will be developed and provided.
Typical working areas are:

1. Assisting the implementation of the Helmholtz Software Policy Template
    * Providing guidance on how to set up or adopt the policy (from individual groups or projects up to individual centres).
    * Supporting the implementation of the policy with guidelines and practical material for the researcher (connection to "Training & Education").
1. Providing Non-binding (legal) advice on Open Source and licenses in general.
1. Supporting the definition of software architectures and related software engineering processes.

### Consultation Hours

In addition to classical consulting, we extend the service into innovative alternative formats like code clinics, hacky hours or hackathons to help with minor questions.
Furthermore, consulting services link to established HIFIS services, for example, training events in correspondence with the demand of the clients.
Our goal is to help scientists to help themselves, including local peer counselling to increase the scalability.

### Knowledge base

In order to provide a sufficient scalability, instruments which are not based on synchronous interactions are needed.
Therefore, a knowledge base will be provided that answers frequently asked questions (FAQs) in the scope of the consulting service.
More information can be found in chapter [Helpful Resources][helpful].

Currently, a two-track approach seems reasonable:

* We use a specific solution for Helmholtz targeted questions (e.g. regarding the Helmholtz Software Policy)
* We promote public platforms like [Stack Overflow][stackoverflow] for general questions and get actively involved there.

Reasons to use a public platform where possible:

1. Questions could have already been answered or will be answered by the public
1. Infrastructure is already setup
1. Visibility of HIFIS

One drawback is that one has to choose carefully because the control of the knowledge is limited, e.g. content could be lost if service closes down.

## Contributors

*in alphabetical order*

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore -->
<table>
<tr>
  <td align="center" width="14.3%"> 
    <a href="https://gitlab.hzdr.de/tfoerst1">
    <img style="min-width: 70px; max-width: 70px;" src="https://gitlab.hzdr.de/uploads/-/system/user/avatar/3852/avatar.png" alt="Thomas"/>
    <p style="font-size:10pt; margin-bottom:-1em; margin-top:0em; font-weight:bold">Thomas <br> Förster </p></a> 
    <br> HZDR <br> Dresden
  </td>
  <td align="center" width="14.3%">
    <a href="https://gitlab.hzdr.de/caha42">
    <img style="min-width: 70px; max-width: 70px;" src="https://gitlab.hzdr.de/uploads/-/system/user/avatar/256/avatar.png" alt="Carina"/>
    <p style="font-size:10pt; margin-bottom:-1em; margin-top:0em; font-weight:bold"> Carina <br> Haupt </p></a> 
    <br> DLR <br> Berlin
  </td>
  <td align="center" width="14.3%">
    <a href="https://gitlab.hzdr.de/frust45">
    <img style="min-width: 70px; max-width: 70px;" src="https://gitlab.hzdr.de/uploads/-/system/user/avatar/4/avatar.png" alt="Tobias">
    <p style="font-size:10pt; margin-bottom:-1em; margin-top:0em; font-weight:bold"> Tobias <br> Huste </p></a> 
    <br> HZDR <br> Dresden
  <td align="center" width="14.3%">
    <a href="https://gitlab.hzdr.de/a.kalali">
    <img style="min-width: 70px; max-width: 70px;" src="https://gitlab.hzdr.de/uploads/-/system/user/avatar/402/avatar.png" alt="Amir">
    <p style="font-size:10pt; margin-bottom:-1em; margin-top:0em; font-weight:bold"> Amir <br> Kalali </p></a> 
    <br> DKFZ <br> Heidelberg
  </td>
  <td align="center" width="14.3%">
    <a href="https://github.com/ASHISRAVINDRAN">
    <img style="min-width: 70px; max-width: 70px;" src="https://gitlab.hzdr.de/uploads/-/system/user/avatar/1555/avatar.png" alt="Ashis"/>
    <p style="font-size:10pt; margin-bottom:-1em; margin-top:0em; font-weight:bold"> Ashis <br> Ravindran </p></a> 
    <br> DKFZ <br> Heidelberg
  </td>
  <td align="center" width="14.3%">
    <a href="https://gitlab.hzdr.de/Drake81">
    <img style="min-width: 70px; max-width: 70px;" src="https://gitlab.hzdr.de/uploads/-/system/user/avatar/1429/avatar.png" alt="Martin"/>
    <p style="font-size:10pt; margin-bottom:-1em; margin-top:0em; font-weight:bold"> Martin <br> Stoffers </p></a> 
    <br> DLR <br> Köln 
   </td>
  <td align="center" width="14.3%">
    <a href="https://gitlab.hzdr.de/benjamin.wolff">
    <img style="min-width: 30px; max-width: 70px;" src="https://gitlab.hzdr.de/uploads/-/system/user/avatar/4872/avatar.png" alt="Benjamin"/>
    <p style="font-size:10pt; margin-bottom:-1em; margin-top:0em; font-weight:bold"> Benjamin <br> Wolff </p></a> 
    <br> DLR <br> Köln
  </td>
</tr>
</table>
<!-- ALL-CONTRIBUTORS-LIST:END --> 

## Feedback

Please [**contact us**](mailto:support@hifis.net) if you are missing any content or if you have general comments.

<!-- LINKS -->
[hifis]: https://hifis.net/
[policy]: https://doi.org/10.48440/os.helmholtz.041
[mission]: https://hifis.net/mission.html
[helpful]: consulting_resources.md
[consulting-pad]: resources/template-consulting-pad.md
[appointment]: resources/template-welcome-note.md
[invite]: resources/template-invite-email.md
[stackoverflow]: https://stackoverflow.com/
[hifis-training]: https://www.hifis.net/services/software/training 
