---
title: Post-Consulting Survey
---

# Using Surveys
Collecting feedback and taking to the clients' perspective is an essential part of any customer-oriented workflow. Since a consultation might
involve asynchronous or dynamic interactions between a consultant and a client, feedback should be collected at the end of any consultation.

## HIFIS Post-Consulting Survey

The feedback process is designed as a survey in HIFIS Consulting.
The following set of questions are put forward to the client in anonymous disposition:

```markdown
1. How did you find out about the software consultation services offered by Helmholtz?
    * HIFIS website (e.g. after reading a blog or article there)
    * HIFIS mailing list
    * Advertised on social media (e.g. Twitter)
    * Advertised via an internal service (e.g. GitLab or notice board system)
    * Mentioned in a talk or workshop
    * Mentioned by a colleague
    * Others:

2. Please rate your experience of the following aspects of the consultation (Very Poor to Excellent)
    * Response time
    * Methods of communication (i.e. emails, video calls, face-to-face meetings, etc.)
    * Relevance of support given during the consultation
    * Impact of the consultation on your project or work

3. How likely are you to recommend using these consultation services to others? (Scale of 0 to 10)

4. Please feel free to share your consultation expectations and experiences or any other comments to help us improve our service.
```

[LimeSurvey](https://en.wikipedia.org/wiki/LimeSurvey), a web server-based software, is used to develop and publish the questionnaire.
The software is hosted and maintained privately by HIFIS. A URL is forwarded to the clients to fill out the survey. Later, the data
is retrieved from the survey administrator for analysis.
