---
title: Publication Policy
---

# Publication Policy 

## Introduction

A consulting service helps researchers to achieve their scientific aims through advising and contributing to their existing work.
Usually, one of the clients' aims in science is publishing research results.
A consultant might end up contributing to a research software which supports activities ranging from the acquisition, processing and analysis of data, to the modeling and the simulation of complex processes.
Hence, software has a significant influence on the quality of research results which implies that the consultant also deserves research credits.
This situation can be a gray area in consulting and therefore it is recommended to formulate and distribute a *Publication Policy* to the client to avoid future conflicts.
A client should be made aware of the policy in the beginning of consulting, viz. during the initial meetings and client's agreement of the policy should be necessary to move further in consulting.
In case a client refuses to agree to the policy, the consultant can choose to advice and work only on non-confronting areas, if any.

## In HIFIS Consulting

HIFIS Consulting has introduced a policy for its consultants which you can refer to [in this chapter](publication_policy.md).
The policy defines guidelines to classify the level of acknowledgment a consultant deserves based on their contributions, ranging from co-authorship or mentioning in the acknowledgments to not needing a mention at all.
