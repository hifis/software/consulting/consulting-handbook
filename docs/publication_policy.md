---
title: HIFIS Publication policy
---

# Publication policy for collaboration with HIFIS Consulting Service 

## Rationale and introduction
The relationship between persons consulting in HIFIS Consulting Service and their clients can best be described as researchers who engage in scientific collaboration with other researchers in Helmholtz.
Any resulting scientific output should, therefore, appropriately reflect the nature of such collaboration and the respective contributions through co-authorship and acknowledgement.
This policy ensures that creative work performed within a consultation, be it designing and writing project-specific code or setting-up research software infrastructure, is adequately appreciated.
It will also ensure the proper acknowledgement of stimuli <!-- Word? --> and original ideas put forward when designing and adapting software as part of collaborative projects that may require domain-specific knowledge provided by the client researcher on the one hand and software engineering knowledge provided by the consultant researcher on the other hand.

In order to ensure a fair publication policy for HIFIS consultants and clients, a contributions made within a consultation are clearly divided into contributions that lead to co-authorship, contributions that lead to mentioning in the acknowledgements, and contributions that do not need any mentioning.
The policy addresses the specific forms of common work and strong dedication of HIFIS consultants in the design and development of software, and providing tools and techniques.

This document sets out the generally binding publication policy for researchers collaborating with HIFIS consultants.
To safeguard good scientific practice, please adhere to the [Guidelines for Safeguarding Good Scientific Practice][DFG2019] provided by the German Research Foundation (DFG - Deutsche Forschungsgemeinschaft) [1].
These guidelines set out the overarching conditions and responsibilities of authorship:

> "An author is an individual who has made a genuine, identifiable contribution to the content of a research publication of text, data or software. All authors agree on the final version of the work to be published. Unless explicitly stated otherwise, they share responsibility for the publication."

The examples given below should provide guidance on how to apply this general principle in the context of the collaboration of HIFIS consultants and other researchers, acknowledging that the judgement of whether a contribution is substantive is ultimately a judgement call and that the lists can never be fully comprehensive.

## (1) Co-authorship contributions
For each publication directly resulting from a consultation, in which a HIFIS consultant was involved, the following creative and analytical contributions must lead to an offer of co-authorship to the contributing HIFIS consultant(s).
Where the involved consultant has contributed according to an item on this list but
declines co-authorship because they themselves do not consider their contribution sufficiently
substantial or because they disagree with the overall conclusion, a mentioning in the
acknowledgements is the appropriate form of giving credit for such efforts.

* **Conceptualisation**: formulation of a conceptual design for a software architecture for the
particular project, e.g.
    1. envisioning solution strategies along with necessary tools for the posed
problem
    1. helping to adjust a solution strategy by identifying severe fundamental
deficiencies that render an existing approach non-functional or that make
envisaged application impossible or severely impaired <!-- What is the difference here? -->

* **Methodology**: development or design of new methodology, e.g.
    1. creating solutions that substantially accelerate existing methods
    1. enabling research experiments that were not possible before due to intractable time for their execution
    1. enabling research experiments that were not possible before due to computing resource limitations
  
* **Software engineering**: if substantial work is carried out in support of the project, e.g.
    1. programming, software development and designing computer programs
    1. implementation of the computer code and supporting algorithms
    1. (automated) testing of existing code components
    1. designing or extending software code <!-- What is the difference between computer code and software code? --> or platforms necessary to solve posed problems

* **Visualisation**: substantive contribution to the design of figure or animation and to the implementation in order to visualise the data or results for a scientific audience
  
* **Writing**: preparation of a publication, specifically writing of substantial sections of the initial draft, e.g. the methodology description

## (2) Acknowledgement contributions
Not sufficient on their own for authorship, but requiring a mention in the
acknowledgements are the following contributions:

* non-trivial input on improvements or implementation of already conceptualised
models and designs
* provision of study materials, computing resources, pipelines or other analysis
tools
* general advice on possible improvements without hands-on work
* support in visualisation, in particular when related to preparing figures in a standardised format without novel concepts and limited in scope
* implementing workflows for testing, validating and improving already existing and functional code
* critical reviewing, commentary or revision of the publication

## Infos and notes

This publication policy is based on the [Publication policy for collaboration with
Helmholtz AI consultants][AI2021] by Helmholtz AI and was adapted by HIFIS Consulting service [1].

To ensure good scientific practice, please adhere to the [Code of Conduct Guidelines
for Safeguarding Good Research Practice][DFG2019] provided by the German Research
Foundation (DFG - Deutsche Forschungsgemeinschaft) [2].


## References


[1] Helmholtz AI. Jan 2021. [Publication policy for collaboration with Helmholtz AI consultants][AI2021]

[2] German Research Foundation (DFG). 2019. [Guidelines for Safeguarding Good Research Practice - Code of Conduct ][DFG2019]

<!-- LINKS -->

[AI2021]: https://voucher-system.helmholtz.ai/static/download_files/2021-01-14_Publication_policy_for_collaboration_with_Helmholtz_AI_consultants_v1.0.pdf

[DFG2019]:https://www.dfg.de/download/pdf/foerderung/rechtliche_rahmenbedingungen/gute_wissenschaftliche_praxis/kodex_gwp_en.pdf
