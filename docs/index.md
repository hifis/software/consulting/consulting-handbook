---
title: Preface
---

# How to start your own Consulting Service

Software consulting is a field of activity that focuses on advising teams in organizations on how best to use information
technology in achieving their research objectives. As an organization becomes larger, software tools and IT infrastructure become important
across teams for an efficient, standardized, error-free and reproducible achievement of their research product.
A consulting service is a team of IT-aware people rendering their advice to different teams in an organisation,
facilitating better use of the digital infrastructure. This could range from fixing code snippets of a macro to rendering
licensing information for the use of certain software.

Through this handbook, we, the HIFIS Consulting team (read more [About Us](./about-us.md)), are sharing our knowledge on founding and operating a consulting service
successfully in the Helmholtz Foundation. This handbook is intended to disseminate information from a process standpoint
so that a reader can get inspired and avoid reinventing the wheel.
The scope of this handbook addresses software engineers, developers or researchers in an organisation who can help other researchers in the area of IT.  
This articulation helps the readers and organisation to get started
with their consulting services, including how to handle consulting requests, pre-and post-consulting preparations, KPIs, as well as on- and offboarding of personnel.

![IT Consulting Teaser](images/it-consulting-teaser.svg){style="display: block; margin: 0 auto; width:85%"}

## Why consulting in the first place?

![IT Consulting Teaser](images/self-vs-consulting.svg){style="display: block; margin: 0 auto; width:85%"}

Suppose, Alex is a scientist. Alex's research entails crunching terabytes of data for a new paper that Alex is writing. However,
Alex is not an inclined computer programmer and finds that the code written for the task doesn't scale. That would mean the paper deadline won't be met. Now, what are Alex's options?  
Either, Alex can improve their coding skills in the direction of scalable computing by themselves and rewrite the code or contact a team of consultants and experts who will help.  
This means multiple people can support the project; effectively providing a peer review.

So, Alex contacts the consulting team, who immediately meet up, audit the software project and propose further helpful actions.
Besides helping Alex with their initial scaling problem, other actions were taken like introducing version control, advising on licensing, and helping to transform their software into a sustainable software beyond personal use.
Consequently, Alex will not only meet deadlines with improved results but will also publishe the code for public use, attaining more research outputs than planned.

Every research objective would require specifically tailored approaches like certain tools or customised process workflows to achieve their aim in a legitimate, bug-free and reproducible manner.
Hence, consultations should be provided, if possible in the realms of licensing, improving software or even setting up new projects.
If feasible, the service should be free of charge as it lowers the barrier to use such offers, especially for time or budget critical projects.

## Overview

All public technical and administrative documentation for software consulting is summarised on this website. This documentation covers various topics from consulting workflows to reporting and post survey for seamlessly running a consulting service through the following main chapters:

- [Consultants][consultants]
- [Consulting Guide][consulting]
- [Helpdesk Guide][helpdesk]
- [Publication Policy][publication]
- [Experts in Consulting][experts]
- [Post Consulting Survey][post]
- [Consulting Reporting][reporting]
- [Helpful Resources][helpful]

The material defines corner-stones, definitions, policies and workflows practiced of consulting. This handbook is intended to be used for software and consultancy services, either as a consultant or an expert. The content is based on experiences made in HIFIS Consulting service.

## Feedback

[![HIFIS Logo with Claim](images/HIFIS_logo_claim_blue.svg){style="align: left; width:55%"}][hifis]

Please [**contact us**](mailto:support@hifis.net) if you are missing any content or if you have general comments.
You may also contribute to this documentation via our Gitlab [repository](https://gitlab.hzdr.de/hifis/software/consulting/hifis-consulting-handbook) (publicly readable, but AAI login required for write access).

## Legal statement

Please note that this handbook and relevant advice are not legal documents and may not take into account all relevant national laws.

<!-- LINKS -->
[consultants]: consultants.md
[consulting]: consulting_guide.md
[helpdesk]: helpdesk_guide.md
[publication]: publication_policy.md
[experts]: experts_in_consulting.md
[post]: post_consulting_survey.md
[reporting]: consulting_reporting.md
[helpful]: consulting_resources.md
[hifis]: https://hifis.net/
