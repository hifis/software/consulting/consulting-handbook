---
title: Reporting
---

# Reporting

## Introduction

Any consultation service requires a self-reflection of its activities for further improvement.
While a post-consulting survey gives a feedback on client experiences, a service should focus on
analysing feedback from consultants for self introspection of its own process and client-facing experience.

Hence, once a consultation is finished, a consultant can be encouraged to create a report
on the key aspects of the consultation. These key aspects should be defined by the services
beforehand for uniform reporting. These can be e.g. start date, end date, efforts put in for
consultation etc.

## Reporting Format

In the HIFIS Consulting, we use a human and machine-readable format for post-consulting reporting.
A report template is defined listing the relevant key aspects for further analysis.
The report is implemented as YAML file wherein a consultant fills out the pre-defined
key aspects, which can then read and processed by a Python script.
All reports are kept in a version control system with a CI pipeline ensuring the integrity of the reports, especially when they are added.

## Report Description

A report should be created by the consultant who was involved during the consultation. The report file is expected to be in
[YAML](https://yaml.org/) format. The report may include the following key points such as, however not limited to:

1. **ticket_number**: The unique id from helpdesk system.
1. **project_name**: The consulting ticket title as written by the client.
1. **consultant_names**: Name(s) of  consultant(s) involved.
1. **client_names**: Name(s) of the client(s) involved.
1. **used_consultation_roles**: List of roles required by the consulation and covered by an consultant.
    1. **Technical domain**: Knowledge about a technical topic, i.e. Testing.
    1. **Tool**: Knowledge about a certain tool, e.g. Gitlab.
    1. **Scientific domain**: Knowledge about the application domain of the software (not IT).
    1. **Organisation**: Knowledge about tools, processes, contact persons, etc. of the organisation.
1. **start_date**: Filing date of the ticket.
1. **end_date**: Date of closure.
1. **workload**:  Approximated final workload (e.g. in hours).
1. **request_types**: Consulting type(s) as per the consulting handbook.
1. **used_technologies**: Technologies or tools used by the consultant to cater the request.
1. **remarks**: Free text describing the consultation story.

The HIFIS Consulting has defined over 20+ fields to be filled in by a consultant for our report creation.

## Analysis

Analysis of the reports can be effortlessly and swiftly conducted using a Python script that parses all the YAML files and computes some basic statistics.
By parsing over a few reports just using the sample template above, interesting insights like average workload across
consultations, monthly inflow of tickets, most frequented request types or technologies, etc. can be deduced quickly.
The "remarks" section can be read manually for a human understanding of what happened during the consultation.
With this information consulting services can prepare e.g. for load balancing, hiring appropriate talents,
proactively notifying experts for help etc.; in addition to self-improving and tuning the consulting processes.

### Process

Assuming [Zammad](helpdesk_guide.md) is the ticket platform used, the following process is defined for reporting:

1. Once a consulting is completed, close the ticket on Zammad and assign the `report-pending` tag.
2. The consultant refers the ticket and prepares the consulting report YAML file and adds the file to the consulting report repository.
3. Once the merge request is accepted change the tag to `report-done`.

## Links

1. Here is a sample template repository with CI/CD, which can be adapted instantly for consulting reports. The repository
also contains minimal python script for further wishful development for report analysis. In addition, there is a linter for
YAML files defined which, is run as a CI/CD pipeline.
Access it here: [https://gitlab.hzdr.de/hifis/software/consulting/consulting-reports-template](
https://gitlab.hzdr.de/hifis/software/consulting/consulting-reports-template)
2. If you are curious on how consulting roles are defined, read more on "Roles in Consulting" [here](https://elib.dlr.de/148644/):  
   Haupt, Carina and Stoffers, Martin; "Roles in Research Software Engineering (RSE) Consultancies", HPC RSE-HPC-2021.
