# Template for a initial welcome message

The goal of the first message is to a establish first a line of communication and second to set up an appointment for a first meeting.

??? tip "Welcome message"

    Dear [first and last name of the client],

    Thank you for the request. We would be happy to support you with your project.
    Let us have an initial call to discuss the details of your project, give feedback, and plan further steps we can take.

    Please select here which appointment fits you best: [web tool to schedule an appoiment] 

    Thanks!

    Best wishes


In Zammad you can create templates for this purpose. In the following one example is shown.

??? tip "Welcome message"
    
    ~~~
    Dear #{ticket.customer.firstname} #{ticket.customer.lastname},
    
    Thank you for the request. We would be happy to support you with your project.
    Let us have an initial call to discuss the details of your project, give feedback, and plan further steps we can take.
    
    Please select here which appointment fits you best: https://terminplaner4.dfn.de
    
    Thanks!
    
    Best wishes
    ~~~
