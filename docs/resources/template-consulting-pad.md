# Template for a collaborated pad

Basis for this pad is a collaboration platform which uses markdown to write documents together.

??? tip "Template pad"

    <h1> [Consulting] 'project name' </h1>
        
    <h2> Participants </h2>
    
    - 'name' ('centre')
    - ...
    
    
    <h2> Background story </h2>
    
    > Information about the project/task.
    
    
    <h2> Resources </h2>
    
    > Information regarding any resources available (gitlab, etc.)
    
    
    <h2> What should be accomplished, Milestones </h2>
    
    > Challenges and Goals, which are needed should be defined here.
    
    
    <h2> Questions </h2>
    
    > Questions regarding the task and challenges.
    
    - [ ] ...
    
    
    <h1> Meeting Notes YYYY-MM-DD </h1>
    
    ...
    
    <h1> Action-Points </h1>
    
    **Client side**
    
    - [ ] ...
    
    **HIFIS side**
    
    - [ ] [Offers for Workshop/Tutorial?][education]
    - [ ] Backup the pad after the meeting.
    - [ ] Annotate pad link in the ticket system
    - [ ] Annotate the backuped pad link
    - [ ] ...
    
    [education]: https://www.helmholtz-hida.de/course-catalog/en/
 

??? tip "Template pad: 'Hedgedoc' (markdown)"

    ~~~ 
    ---
    tags  : consulting, idxxxx, template
    title : Consulting - 'project name' 
    ---
    
    # [Consulting] 'project name'
    
    ::: info
    ## Participants
    
    - 'name' ('centre')
    - ...
    
    
    ## Background story
    
    > Information about the project/task.
    
    
    ## Resources
    
    > Information regarding any resources available (gitlab, etc.)
    
    
    ## What should be accomplished, Milestones
    
    > Challenges and Goals, which are needed should be defined here.
    
    
    ## Questions
    
    > Questions regarding the task and challenges.
    
    - [ ] ...
    
    :::
    
    # Meeting Notes YYYY-MM-DD
    
    
    # Action-Points
    
    **Client side**
    
    - [ ] ...
    
    **HIFIS side**
    
    - [ ] [Offers for Workshop/Tutorial?][education]
    - [ ] Backup the pad after the meeting.
    - [ ] Annotate pad link in the ticket system
    - [ ] Annotate the backuped pad link
    - [ ] ...
    
    [education]: https://www.helmholtz-hida.de/course-catalog/en/
    ~~~

