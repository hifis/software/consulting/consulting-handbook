---
title: Helpful Resources
---

[![HIFIS-Logo](images/hifis-logo.png)](https://hifis.net)

# Consulting Resources

This chapter contains all internal resources needed for consulting and mentioned throughout this handbook.

## Awesome-RSE List

The awesome-rse list is a curated awesome list of resources for Research Software Engineering (RSE).
The list contains resources which can potentially be recommended to a consulting client.

[https://github.com/hifis-net/awesome-rse](https://github.com/hifis-net/awesome-rse)

## Templates

- [Collaboration pads](resources/template-consulting-pad.md); to document the effort and provide a stable means of exchanging links and TODO's.
- [Initial meeting invite](resources/template-welcome-note.md); to invite clients to the first consultation.
